function [cost,grad] = sparseAutoencoderCost(theta, visibleSize, hiddenSize, ...
    lambda, sparsityParam, beta, data)

% visibleSize: the number of input units (probably 64)
% hiddenSize: the number of hidden units (probably 25)
% lambda: weight decay parameter
% sparsityParam: The desired average activation for the hidden units (denoted in the lecture
%                           notes by the greek alphabet rho, which looks like a lower-case "p").
% beta: weight of sparsity penalty term
% data: Our 64x10000 matrix containing the training data.  So, data(:,i) is the i-th training example.

% The input theta is a vector (because minFunc expects the parameters to be a vector).
% We first convert theta to the (W1, W2, b1, b2) matrix/vector format, so that this
% follows the notation convention of the lecture notes.

W1 = reshape(theta(1:hiddenSize*visibleSize), hiddenSize, visibleSize);
W2 = reshape(theta(hiddenSize*visibleSize+1:2*hiddenSize*visibleSize), visibleSize, hiddenSize);
b1 = theta(2*hiddenSize*visibleSize+1:2*hiddenSize*visibleSize+hiddenSize);
b2 = theta(2*hiddenSize*visibleSize+hiddenSize+1:end);

% Cost and gradient variables (your code needs to compute these values).
% Here, we initialize them to zeros.
cost = 0;
W1grad = zeros(size(W1));
W2grad = zeros(size(W2));
b1grad = zeros(size(b1));
b2grad = zeros(size(b2));


M=size(data,2); % M is dataset size.
x = data;

z2=W1*x+repmat(b1,1,M);
a2=1./(1+exp(-z2));
z3=W2*a2+repmat(b2,1,M);
y=1./(1+exp(-z3));
p_hat=sum(a2,2)./M;

cost = 1/2*sum(sum((y - data).^2))/M + 1/2*lambda*(sum(sum(W1.^2))+sum(sum(W2.^2))) + beta*sum(sparsityParam.*log(sparsityParam./p_hat) + (1-sparsityParam).*log((1-sparsityParam)./(1-p_hat)));

det3 = -(x-y).*y.*(1-y);
det2 = (W2'*det3 + beta*(repmat((-sparsityParam./p_hat + (1-sparsityParam)./(1-p_hat)),1,M))).*a2.*(1-a2);
W1grad = det2*x';
W2grad = det3*a2';
b1grad = sum(det2,2);
b2grad = sum(det3,2);

W1grad = W1grad/M+lambda*W1;
W2grad = W2grad/M+lambda*W2;
b1grad = b1grad/M;
b2grad = b2grad/M;


%-------------------------------------------------------------------
% After computing the cost and gradient, we will convert the gradients back
% to a vector format (suitable for minFunc).  Specifically, we will unroll
% your gradient matrices into a vector.

grad = [W1grad(:) ; W2grad(:) ; b1grad(:) ; b2grad(:)];

end

%-------------------------------------------------------------------
% Here's an implementation of the sigmoid function
function sigm = sigmoid(x)

sigm = 1 ./ (1 + exp(-x));
end

